import csv
import os
import pandas as pd
from itertools import count
from faker import Faker

fake_data = Faker()

def reMap(fieldnames, duplicatedlist):
    for i in duplicatedlist:
        return [i if f.startswith(i) else f
            for f in fieldnames]

def getFileName(iFile):
    fname = "de_identify" + os.path.basename(iFile)
    return fname

def deIdentify(iFile, oFile):
    csv_reader = pd.read_csv(iFile)
    pri_identifier = []
    duplicatedlist = ['subscriber_address_line']
    fieldnames = reMap(csv_reader.columns, duplicatedlist)

    for i in csv_reader.index:
        csv_reader.at[i, 'subscriber_last_name'] = fake_data.last_name()
        csv_reader.at[i, 'subscriber_first_name'] = fake_data.first_name()
        csv_reader.at[i, 'subscriber_middle_name_or_initial'] = fake_data.random_element(elements=('M', 'L', 'P', 'Q',''))
        csv_reader.at[i, 'subscriber_name_suffix'] =  fake_data.suffix()
        pri_id = fake_data.random_number(digits = 10, fix_len = True)
        while pri_id in pri_identifier:
            pri_id = fake_data.random_number(digits = 10, fix_len = True)
        csv_reader.at[i,'subscriber_primary_identifier'] = pri_id
        pri_identifier.append(pri_id)
        csv_reader.at[i, 'subscriber_city_name'] = fake_data.city()
        csv_reader.at[i, 'subscriber_state_code'] = fake_data.military_state()
        csv_reader.at[i, 'subscriber_postal_zone_or_zip_code'] = fake_data.postalcode_plus4()
        csv_reader.at[i, 'subscriber_birth_date'] = fake_data.date_of_birth(minimum_age=41)
        csv_reader.at[i, 'subscriber_gender_code'] = fake_data.random_element(elements=('M', 'F'))
        csv_reader.at[i, 'subscriber_address_line'] = fake_data.address()
        csv_reader.at[i, 'subscriber_address_line.1'] = fake_data.secondary_address()
    csv_reader.columns = fieldnames
    csv_reader.to_csv(oFile, index = False)
    
if __name__  == '__main__':
    deIdentify("../input/2010BA_DEIDENT_INTHB0837I_286230_10160249.csv","../output/" + getFileName("../input/2010BA_DEIDENT_INTHB0837I_286230_10160249.csv"))
    deIdentify("../input/2010BA_DEIDENT_INTPSV837P_286193_10140229.csv","../output/" + getFileName("../input/2010BA_DEIDENT_INTPSV837P_286193_10140229.csv"))