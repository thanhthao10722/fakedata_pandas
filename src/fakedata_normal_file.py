from faker import Faker
import pandas as pd
import os.path

fake_data = Faker()

def fake_row(string):
    data = string.split('*', 9)
    data[3] = fake_data.first_name()
    data[4] = fake_data.last_name()
    data[9] = str(fake_data.random_number(digits = 10, fix_len = True))
    a = '*'.join(data) + '~'
    return a

def getFileName(iFile):
    fname = "de_identify" + os.path.basename(iFile)
    return fname

def deIdentify(iFile,oFile):
    normal_reader = pd.read_csv(iFile)
    for i in normal_reader.index:
        normal_reader.iloc[i] = normal_reader.iloc[i].apply(fake_row)
    normal_reader.to_csv(oFile, index = False, header = None)

if __name__ == '__main__':
    deIdentify("../input/subscriber_name.txt", "../output/" + getFileName("../input/subscriber_name.txt"))